﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ParticleTrackerView : MonoBehaviour
{

    public static Texture2D[] Texture2Ds;

    [NonSerialized]
    public ParticlesTrackableController[] Controllers;

    public event Action<bool> CritDistance;

    private const int CRIT_DISTANCE = 150;

    private List<ParticlesTrackableController> _trackableEventSystems;

    private ParticleSceneController _particleSceneController;

    private float distance;
    void Start()
    {
    
        Texture2Ds = Resources.LoadAll<Texture2D>("Textures");

        Controllers = FindObjectsOfType<ParticlesTrackableController>();

        _particleSceneController = new ParticleSceneController(this);

    }

    void Update()
    {
        if (_particleSceneController != null)
        {
            _particleSceneController.Rotation();
        }
    }

    void OnGUI()
    {

        if (_particleSceneController != null)
        {
            distance = _particleSceneController.Distance();

            GUI.Label(new Rect(20, 20, 500, 30), string.Format("distance: {0}", distance));

            if (_particleSceneController.Count())
            {
                if ((distance < CRIT_DISTANCE && distance > 0))
                {
                    if (CritDistance != null)
                    {
                        CritDistance(true);
                    }
                }
                else
                {
                    if (CritDistance != null)
                    {
                        CritDistance(false);
                    }
                }
            }
            else
            {
                if (CritDistance != null)
                {
                    CritDistance(false);
                }
            }
        }

    }
}

