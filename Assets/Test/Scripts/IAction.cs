﻿using UnityEngine;

interface IAction
{
    void StartAction();

    void StopAction();

    void RotationTo(Vector3 dir);

    void RotationTo(float angle);

    void DefaultRotation();
}
