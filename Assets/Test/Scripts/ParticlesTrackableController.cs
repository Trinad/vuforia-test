﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Vuforia;


public class ParticlesTrackableController : MonoBehaviour, ITrackableEventHandler, IAction
{
    public bool isBeingTracked;

    public event Action<ParticlesTrackableController> Change;

    private TrackableBehaviour mTrackableBehaviour;

    private ParticleSystem particleSystem;

    const int SPHERE_RANDOM_FROM = -50;
    const int SPHERE_RANDOM_TO = 50;

    #region Unity
    void Start()
    {
        particleSystem = GetComponentInChildren<ParticleSystem>();

        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }

        spheres = new List<GameObject>();
    }
    #endregion

    #region ITrackableEventHandler
    /// <summary>
    /// Implementation of the ITrackableEventHandler function called when the
    /// tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
                                    TrackableBehaviour.Status previousStatus,
                                    TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            isBeingTracked = true;

            if (Change != null)
            {
                Change(this);
            }
        }
        else
        {
            isBeingTracked = false;

            if (Change != null)
            {
                Change(this);
            }
        }
    }
    #endregion

    #region IAction
    public void StartAction()
    {
        particleSystem.Play();
        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
    }

    public void StopAction()
    {
        particleSystem.Stop();
        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
    }

    public void RotationTo(Vector3 dir)
    {
        particleSystem.transform.LookAt(dir);
    }

    public void RotationTo(float angle)
    {

    }

    public void DefaultRotation()
    {
        particleSystem.transform.forward = transform.up;
    }

    #endregion

    #region Shperes
    private List<GameObject> spheres;
    public void Spheres(bool isCreated, int count, float radius)
    {
        if (isCreated)
        {
            if (spheres.Count < count)
            {
                for (int i = 0; i < count; i++)
                {

                    var sphere = SphereCreatin(radius);

                    sphere.transform.position = new Vector3(
                        transform.position.x + UnityEngine.Random.Range(SPHERE_RANDOM_FROM, SPHERE_RANDOM_TO),
                        transform.position.y + UnityEngine.Random.Range(SPHERE_RANDOM_FROM, SPHERE_RANDOM_TO),
                        transform.position.z + UnityEngine.Random.Range(SPHERE_RANDOM_FROM, SPHERE_RANDOM_TO)
                        );

                    sphere.transform.parent = transform;

                    spheres.Add(sphere);
                }
            }
        }
        else
        {
            foreach (var sphere in spheres)
            {
                Destroy(sphere);
            }

            spheres.Clear();
        }
    }

    GameObject SphereCreatin(float radius)
    {
        var sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);

        sphere.transform.localScale = new Vector3(radius, radius, radius);

        var texture = ParticleTrackerView.Texture2Ds[UnityEngine.Random.Range(0, ParticleTrackerView.Texture2Ds.Length)];

        var render = sphere.GetComponent<Renderer>();

        render.material.mainTexture = texture;

        render.material.shader = Shader.Find("Mobile/Diffuse");

        return sphere;
    }

    #endregion
}
