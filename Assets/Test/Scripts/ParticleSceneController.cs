﻿using UnityEngine;
using System.Collections.Generic;


public class ParticleSceneController
{
    public int CurrentTrackableCount;

    private const int SPHERE_COUNT = 25;

    private const int SPHERE_RADIUS = 10;

    private ParticlesTrackableController[] _controllers;

    private List<ParticlesTrackableController> _currentTrackable;

    public ParticleSceneController(ParticleTrackerView view)
    {
        this._controllers = view.Controllers;

        _currentTrackable = new List<ParticlesTrackableController>();

        foreach (var controller in _controllers)
        {
            controller.Change += TrackableController_Change;
        }

        view.CritDistance += View_CritDistance;
    }

    private void View_CritDistance(bool isCreated)
    {
        foreach (var system in _controllers)
        {
            system.Spheres(isCreated, SPHERE_COUNT, SPHERE_RADIUS);
        }
    }

    private void TrackableController_Change(ParticlesTrackableController obj)
    {
        if (obj.isBeingTracked)
        {
            _currentTrackable.Add(obj);

            obj.StartAction();
        }
        else
        {
            obj.StopAction();

            _currentTrackable.Remove(obj);
        }

        CurrentTrackableCount = _currentTrackable.Count;
    }

    public bool Count()
    {
        return Count(_currentTrackable);
    }
    private bool Count(List<ParticlesTrackableController> controllers)
    {
        if (controllers.Count > 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public void Rotation()
    {
        if (Count(_currentTrackable))
        {
            _currentTrackable[0].RotationTo(_currentTrackable[1].transform.position);

            _currentTrackable[1].RotationTo(_currentTrackable[0].transform.position);
        }
        else
        {
            foreach (var system in _controllers)
            {
                system.DefaultRotation();
            }
        }
    }

    public float Distance()
    {
        float distance = 0;
        if (Count(_currentTrackable))
        {
            distance = Vector3.Distance(_currentTrackable[1].transform.position, _currentTrackable[0].transform.position);

            return distance;
        }
        return distance;
    }
}
